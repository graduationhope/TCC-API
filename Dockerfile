# Especifica a imagem pai.
FROM debian:stretch

# Configura o diretório de trabalho para /TCC-API.
# Todos os comandos subsequentes serão efetuados neste diretório.
WORKDIR /TCC-API/application

# Copia o conteúdo do diretório atual /TCC-API para dentro do container
# em um diretório com o mesmo nome /TCC-API.
COPY . /TCC-API

# Instalação e configuração.
RUN apt update \
	&& apt install --yes openjdk-8-jdk

# Configura variável de ambiente para execução do servidor web.
ENV PATH=$PATH:/TCC-API/application

# Expõem a porta 8080 no mundo do lado de fora deste container.
EXPOSE 8080

# Executa o servidor web.
CMD ["sh", "-c", "gradlew bootRun"]
