package application.database.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Equipment {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = true)
	private String acronym;

	@Column(nullable = false)
	private String whatis;

	@Column(nullable = false)
	private String function;

	@Column(nullable = false)
	private String howtouse;

	@Column(nullable = true)
	private String curiosities;
}

