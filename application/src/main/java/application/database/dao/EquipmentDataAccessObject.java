package application.database.dao;

import application.database.entity.Equipment;

import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import org.springframework.boot.CommandLineRunner;

@Component
public class EquipmentDataAccessObject implements CommandLineRunner {
	private final JdbcTemplate template;

	@Autowired
	public EquipmentDataAccessObject(JdbcTemplate template) {
		this.template = template;
	}

	public List select() {
		String sql = "SELECT name FROM equipment";
		return template.queryForList(sql);
	}

	public void run(String... args) {
		List<Equipment> list = this.select();
		for (int index = 0; index < list.size(); index++) {
			System.out.println(list.get(index) + "\n");
		}
	}
}
