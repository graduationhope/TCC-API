package application.pages;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping(path = "/hello")
	public String hello() {
		return "\nOlá, Mundo\n\n";
	}
}
