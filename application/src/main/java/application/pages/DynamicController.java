package application.pages;



// Controller
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

// View
import org.springframework.ui.Model;

// Model
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import application.database.entity.Equipment;

@Controller
public class DynamicController {

	private final JdbcTemplate template;

	@Autowired
	public DynamicController(JdbcTemplate template) {
		this.template = template;
	}

	@RequestMapping(path = "/dynamic")
	public String dynamic(Model model) {
		String sql = "SELECT * FROM equipment;";
		List list = template.queryForList(sql);
		model.addAttribute("list", list);
		return "dynamic";
	}
}
